#include <stdio.h>

#define SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

int main()
{
	int arr[5]={1,2,3,4,5};
	int *p=arr;
	char text;
	printf("To get the size of the array please type SIZE(arr)\n");
	scanf("%c", &text);
	printf("Size of the array is: %d", SIZE(arr));
	
	return 0;
}




